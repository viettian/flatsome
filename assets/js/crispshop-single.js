
jQuery(document).ready(function () {
	jQuery('.open-popup-checkout').click(function (e) {
		e.preventDefault();
		jQuery(this).addClass('loading');
		var product_id = jQuery(this).data('id');
		var quantity = jQuery('input[name="quantity"]').val();
		var datahref = jQuery(this).data('href');
		jQuery.ajax({
			url : crispshop_ajax_object.ajax_url,
			type: 'POST',
			data: 'action=crispshop_add_cart_single&product_id=' + product_id + '&quantity=' + quantity,

			success: function (data) {
				setTimeout(function () {
					//jQuery('.open-popup-checkout').removeClass('loading');
					window.location.href = datahref;
				}, 3000);
			}
		});
	});
	jQuery('.add-to-cart-button').click(function (e) {
		e.preventDefault();
		jQuery(this).addClass('loading');
		var product_id = jQuery(this).data('id');
		var quantity = jQuery('input[name="quantity"]').val();
		var datahref = jQuery(this).data('href');
		jQuery.ajax({
			url : crispshop_ajax_object.ajax_url,
			type: 'POST',
			data: 'action=crispshop_add_cart_single&product_id=' + product_id + '&quantity=' + quantity,

			success: function (data) {
				setTimeout(function () {
					// jQuery('.add-to-cart-button').removeClass('loading');
					window.location.href = datahref;
				}, 3000);
			}
		});
	});
});

