<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */

require get_template_directory() . '/inc/init.php';

/**
 * Note: It's not recommended to add any custom code here. Please use a child theme so that your customizations aren't
 * lost during updates. Learn more here: http://codex.wordpress.org/Child_Themes
 */
function crp_force_display( $result, $product_id ) {
	$related_ids = get_post_meta( $product_id, '_related_ids', true );

	return empty( $related_ids ) ? $result : true;
}

add_filter( 'woocommerce_product_related_posts_force_display', 'crp_force_display', 10, 2 );

/**
 * Determine whether we want to consider taxonomy terms when selecting related products.
 * This is required for WooCommerce 3.0.
 *
 * @param bool $result     Whether or not we should consider tax terms during selection.
 * @param int  $product_id The ID of the current product.
 *
 * @return bool Modified value - should we consider tax terms during selection?
 */
function crp_taxonomy_relation( $result, $product_id ) {
	$related_ids = get_post_meta( $product_id, '_related_ids', true );
	if( !empty( $related_ids ) ) {
		return false;
	} else {
		return 'none' === get_option( 'crp_empty_behavior' ) ? false : $result;
	}
}

add_filter( 'woocommerce_product_related_posts_relate_by_category', 'crp_taxonomy_relation', 10, 2 );
add_filter( 'woocommerce_product_related_posts_relate_by_tag', 'crp_taxonomy_relation', 10, 2 );

/**
 * Add related products selector to product edit screen
 */
function crp_select_related_products() {
	global $post, $woocommerce;
	$product_ids = array_filter( array_map( 'absint', (array) get_post_meta( $post->ID, '_related_ids', true ) ) );
	?>
	<div class="options_group">
		<?php if( $woocommerce->version >= '3.0' ) : ?>
			<p class="form-field">
				<label for="related_ids"><?php _e( 'Related Products', 'woocommerce' ); ?></label>
				<select class="wc-product-search" multiple="multiple" style="width: 50%;" id="related_ids" name="related_ids[]" data-placeholder="<?php esc_attr_e( 'Search for a product&hellip;', 'woocommerce' ); ?>" data-action="woocommerce_json_search_products_and_variations" data-exclude="<?php echo intval( $post->ID ); ?>">
					<?php
					foreach ( $product_ids as $product_id ) {
						$product = wc_get_product( $product_id );
						if( is_object( $product ) ) {
							echo '<option value="' . esc_attr( $product_id ) . '"' . selected( true, true, false ) . '>' . wp_kses_post( $product->get_formatted_name() ) . '</option>';
						}
					}
					?>
				</select> <?php echo wc_help_tip( __( 'Related products are displayed on the product detail page.', 'woocommerce' ) ); ?>
			</p>
		<?php elseif( $woocommerce->version >= '2.3' ) : ?>
			<p class="form-field"><label for="related_ids"><?php _e( 'Related Products', 'woocommerce' ); ?></label>
				<input type="hidden" class="wc-product-search" style="width: 50%;" id="related_ids" name="related_ids" data-placeholder="<?php _e( 'Search for a product&hellip;', 'woocommerce' ); ?>" data-action="woocommerce_json_search_products" data-multiple="true" data-selected="<?php
				$json_ids = array();
				foreach ( $product_ids as $product_id ) {
					$product = wc_get_product( $product_id );
					if( is_object( $product ) && is_callable( array( $product, 'get_formatted_name' ) ) ) {
						$json_ids[$product_id] = wp_kses_post( $product->get_formatted_name() );
					}
				}

				echo esc_attr( json_encode( $json_ids ) );
				?>" value="<?php echo implode( ',', array_keys( $json_ids ) ); ?>" />
				<img class="help_tip" data-tip='<?php _e( 'Related products are displayed on the product detail page.', 'woocommerce' ) ?>' src="<?php echo WC()->plugin_url(); ?>/assets/images/help.png" height="16" width="16" />
			</p>
		<?php else: ?>
			<p class="form-field"><label for="related_ids"><?php _e( 'Related Products', 'woocommerce' ); ?></label>
				<select id="related_ids" name="related_ids[]" class="ajax_chosen_select_products" multiple="multiple" data-placeholder="<?php _e( 'Search for a product&hellip;', 'woocommerce' ); ?>">
					<?php
					foreach ( $product_ids as $product_id ) {

						$product = get_product( $product_id );

						if( $product ) {
							echo '<option value="' . esc_attr( $product_id ) . '" selected="selected">' . esc_html( $product->get_formatted_name() ) . '</option>';
						}
					}
					?>
				</select>
				<img class="help_tip" data-tip='<?php _e( 'Related products are displayed on the product detail page.', 'woocommerce' ) ?>' src="<?php echo WC()->plugin_url(); ?>/assets/images/help.png" height="16" width="16" />
			</p>
		<?php endif; ?>
	</div>
	<?php
}

add_action( 'woocommerce_product_options_related', 'crp_select_related_products' );

/**
 * Save related products selector on product edit screen.
 *
 * @param int $post_id ID of the post to save.
 * @param     obj      WP_Post object.
 */
function crp_save_related_products( $post_id, $post ) {
	global $woocommerce;
	if( isset( $_POST['related_ids'] ) ) {
		// From 2.3 until the release before 3.0 Woocommerce posted these as a comma-separated string.
		// Before and after, they are posted as an array of IDs.
		if( $woocommerce->version >= '2.3' && $woocommerce->version < '3.0' ) {
			$related = isset( $_POST['related_ids'] ) ? array_filter( array_map( 'intval', explode( ',', $_POST['related_ids'] ) ) ) : array();
		} else {
			$related = array();
			$ids     = $_POST['related_ids'];
			foreach ( $ids as $id ) {
				if( $id && $id > 0 ) {
					$related[] = absint( $id );
				}
			}
		}
		update_post_meta( $post_id, '_related_ids', $related );
	} else {
		delete_post_meta( $post_id, '_related_ids' );
	}
}

add_action( 'woocommerce_process_product_meta', 'crp_save_related_products', 10, 2 );

/**
 * Filter the related product query args.
 * This function works for WooCommerce prior to 3.0.
 *
 * @param array $args Query arguments.
 *
 * @return array Modified query arguments.
 */
function crp_filter_related_products_legacy( $args ) {
	global $post;
	$related = get_post_meta( $post->ID, '_related_ids', true );
	if( $related ) { // remove category based filtering
		$args['post__in'] = $related;
	} elseif( get_option( 'crp_empty_behavior' ) == 'none' ) { // don't show any products
		$args['post__in'] = array( 0 );
	}

	return $args;
}

add_filter( 'woocommerce_related_products_args', 'crp_filter_related_products_legacy' );

/**
 * Filter the related product query args.
 *
 * @param array $query      Query arguments.
 * @param int   $product_id The ID of the current product.
 *
 * @return array Modified query arguments.
 */
function crp_filter_related_products( $query, $product_id ) {
	$related_ids = get_post_meta( $product_id, '_related_ids', true );
	if( !empty( $related_ids ) && is_array( $related_ids ) ) {
		$related_ids = implode( ',', array_map( 'absint', $related_ids ) );
		$query['where'] .= " AND p.ID IN ( {$related_ids} )";
	}

	return $query;
}

add_filter( 'woocommerce_product_related_posts_query', 'crp_filter_related_products', 20, 2 );


/**
 * Create the menu item.
 */
function crp_create_menu() {
	add_submenu_page( 'woocommerce', 'Custom Related Products', 'Custom Related Products', 'manage_options', 'custom_related_products', 'crp_settings_page' );
}

add_action( 'admin_menu', 'crp_create_menu', 99 );

/**
 * Create the settings page.
 */
function crp_settings_page() {
	if( isset( $_POST['submit_custom_related_products'] ) && current_user_can( 'manage_options' ) ) {
		check_admin_referer( 'custom_related_products', '_custom_related_products_nonce' );

		// save settings
		if( isset( $_POST['crp_empty_behavior'] ) && $_POST['crp_empty_behavior'] != '' ) {
			update_option( 'crp_empty_behavior', $_POST['crp_empty_behavior'] );
		} else {
			delete_option( 'crp_empty_behavior' );
		}

		echo '<div id="message" class="updated"><p>Settings saved</p></div>';
	}

	?>
	<div class="wrap" id="custom-related-products">
		<h2>Custom Related Products</h2>
		<?php
		$behavior_none_selected = ( get_option( 'crp_empty_behavior' ) == 'none' ) ? 'selected="selected"' : '';

		echo '
		<form method="post" action="admin.php?page=custom_related_products">
			' . wp_nonce_field( 'custom_related_products', '_custom_related_products_nonce', true, false ) . '
			<p>If I have not selected related products:
				<select name="crp_empty_behavior">
					<option value="">Select random related products by category</option>
					<option value="none" ' . $behavior_none_selected . '>Don&rsquo;t show any related products</option>
				</select>
			</p>
			<p>
				<input type="submit" name="submit_custom_related_products" value="Save" class="button button-primary" />
			</p>
		</form>
	';
		?>
	</div>

	<?php
} // end settings page

if( !function_exists( 'vi_button_add_to_cat' ) ) {
	function vi_button_add_to_cat() {
		global $woocommerce;
		$checkout_url = $woocommerce->cart->get_checkout_url();
		$html         = '<a class="open-popup-checkout ql-button button alt" data-id="' . get_the_ID() . '" data-href="' . $checkout_url . '" href="jvascript:;">Mua trả góp</a>';
		$html         .= '<a class="add-to-cart-button ql-button button alt" data-id="' . get_the_ID() . '" data-href="'.get_the_permalink(get_the_ID()).'" href="jvascript:;">Thêm vào giỏ</a>';
		echo $html;
	}
}
//add_action('woocommerce_single_product_summary','vi_button_add_to_cat',35);

if( !function_exists( 'popup_checkout_html' ) ) {
	function popup_checkout_html() {
		wc_get_template_part( 'single-product/popup-checkout' );
	}
}
add_action( 'wp_footer', 'popup_checkout_html' );

//add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
//	unset($fields['billing']['billing_postcode']);
//	unset($fields['billing']['billing_address_2']);
//	unset($fields['billing']['billing_company']);
//	unset($fields['billing']['billing_city']);
//	unset($fields['billing']['billing_country']);
//	unset($fields['billing']['billing_state']);
//
//	unset($fields['shipping']['shipping_company']);
//	unset($fields['shipping']['shipping_address_2']);
//	unset($fields['shipping']['billing_city']);
//	unset($fields['shipping']['billing_country']);
//	unset($fields['shipping']['billing_state']);
//	unset($fields['shipping']['shipping_postcode']);
//	return $fields;
}

function crispshop_scripts() {
	if( is_singular( 'product' ) ) {
		wp_enqueue_script( 'crispshop-single', get_template_directory_uri() . '/assets/js/crispshop-single.js', array( 'jquery' ), '1.0.0', true );
		wp_localize_script( 'crispshop-single', 'crispshop_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}
}

add_action( 'wp_enqueue_scripts', 'crispshop_scripts' );

function crispshop_add_cart_single_ajax() {
	$product_id   = $_POST['product_id'];
	$variation_id = $_POST['variation_id'];
	$quantity     = $_POST['quantity'];

	if( $variation_id ) {
		WC()->cart->add_to_cart( $product_id, $quantity, $variation_id );
	} else {
		WC()->cart->add_to_cart( $product_id, $quantity );
	}
	die();
}

add_action( 'wp_ajax_crispshop_add_cart_single', 'crispshop_add_cart_single_ajax' );
add_action( 'wp_ajax_nopriv_crispshop_add_cart_single', 'crispshop_add_cart_single_ajax' );

//add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);

//function change_existing_currency_symbol( $currency_symbol, $currency ) {
//	switch( $currency ) {
//		case 'VND': $currency_symbol = 'VNĐ'; break;
//	}
//	return $currency_symbol;
//}


// Display Fields
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );

// Save Fields
add_action( 'woocommerce_process_product_meta', 'woo_add_custom_general_fields_save' );


function woo_add_custom_general_fields() {

	global $woocommerce, $post;

	echo '<div class="options_group">';

	woocommerce_wp_text_input(
		array(
			'id'          => 'sale_text',
			'label'       => __( 'Khuyến mãi', 'woocommerce' ),
			'placeholder' => '',
			'desc_tip'    => 'true',
			'description' => __( 'Nhập khuyến mãi.', 'woocommerce' )
		)
	);
	echo '</div>';

}

function woo_add_custom_general_fields_save( $post_id ) {

	// Text Field
	$woocommerce_text_field = $_POST['sale_text'];
	if( !empty( $woocommerce_text_field ) ) {
		update_post_meta( $post_id, 'sale_text', esc_attr( $woocommerce_text_field ) );
	}

}

add_filter( 'add_to_cart_text', 'woo_custom_single_add_to_cart_text' );                // < 2.1
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text' );  // 2.1 +

function woo_custom_single_add_to_cart_text() {

	return __( 'Mua ngay', 'woocommerce' );

}

add_filter( 'woocommerce_add_to_cart_redirect', 'woo_redirect_to_checkout' );
function woo_redirect_to_checkout() {
	$checkout_url = WC()->cart->get_checkout_url();

	return $checkout_url;
}


//require_once(get_template_directory() . '/inc/add_payment.php');
function woo_add_single_sale() {
	global $product;
	$sale_text = get_field( 'single_sale_text', $product->id );
	if( $sale_text ) {
		echo '<div class="sale-sing-text-wrapper">';

		echo $sale_text;

		echo '</div>';
	}
}

add_action( 'woocommerce_single_product_summary', 'woo_add_single_sale', 20 );


add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

	unset( $tabs['additional_information'] );    // Remove the additional information tab

	return $tabs;

}


add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {

	$tabs['reviews']['priority']     = 30;            // Reviews first
	$tabs['description']['priority'] = 5;            // Description second

	return $tabs;
}

add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['description']['title'] = __( 'Ưu điểm nổi bật' );

	return $tabs;

}

function remove_comment_fields( $fields ) {
	unset( $fields['url'] );
	unset( $fields['email'] );

	return $fields;
}

add_filter( 'comment_form_default_fields', 'remove_comment_fields' );


function post_related_product() {
	global $product;
	$list_post = get_field( 'custom_related_post',$product->id );
	if( $list_post ) :
		echo '<div class="product-ralated-post">';
		echo '<h3 class="product-section-title product-section-title-related pt-half pb-half uppercase">Tư vấn sử dụng</h3>';
		echo '<ul>';
		foreach ( $list_post as $item ) {
			echo '<li><a href="' . get_permalink( $item->ID ) . '">' . get_the_title( $item->ID ) . '</a> </li>';
		}
		echo '</ul>';
		echo '</div>';
	endif;
}
add_action( 'woocommerce_after_single_product_summary','post_related_product',10 );

// Remove product cat base
add_filter('term_link', 'devvn_no_term_parents', 1000, 3);
function devvn_no_term_parents($url, $term, $taxonomy) {
	if($taxonomy == 'product_cat'){
		$term_nicename = $term->slug;
		$url = trailingslashit(get_option( 'home' )) . user_trailingslashit( $term_nicename, 'category' );
	}
	return $url;
}

// Add our custom product cat rewrite rules
function devvn_no_product_cat_parents_rewrite_rules($flash = false) {
	$terms = get_terms( array(
		'taxonomy' => 'product_cat',
		'post_type' => 'product',
		'hide_empty' => false,
	));
	if($terms && !is_wp_error($terms)){
		foreach ($terms as $term){
			$term_slug = $term->slug;
			add_rewrite_rule($term_slug.'/?$', 'index.php?product_cat='.$term_slug,'top');
			add_rewrite_rule($term_slug.'/page/([0-9]{1,})/?$', 'index.php?product_cat='.$term_slug.'&paged=$matches[1]','top');
			add_rewrite_rule($term_slug.'/(?:feed/)?(feed|rdf|rss|rss2|atom)/?$', 'index.php?product_cat='.$term_slug.'&feed=$matches[1]','top');
		}
	}
	if ($flash == true)
		flush_rewrite_rules(false);
}
add_action('init', 'devvn_no_product_cat_parents_rewrite_rules');

/*Sửa lỗi khi tạo mới taxomony bị 404*/
add_action( 'create_term', 'devvn_new_product_cat_edit_success', 10, 2 );
function devvn_new_product_cat_edit_success( $term_id, $taxonomy ) {
	devvn_no_product_cat_parents_rewrite_rules(true);
}
/*
* Code Bỏ /product/ hoặc /cua-hang/ hoặc /shop/ ... có hỗ trợ dạng %product_cat%
* Thay /cua-hang/ bằng slug hiện tại của bạn
*/
function devvn_remove_slug( $post_link, $post ) {
	if ( !in_array( get_post_type($post), array( 'product' ) ) || 'publish' != $post->post_status ) {
		return $post_link;
	}
	if('product' == $post->post_type){
		$post_link = str_replace( '/san-pham/', '/', $post_link ); //Thay cua-hang bằng slug hiện tại của bạn
	}else{
		$post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
	}
	return $post_link;
}
add_filter( 'post_type_link', 'devvn_remove_slug', 10, 2 );
/*Sửa lỗi 404 sau khi đã remove slug product hoặc cua-hang*/
function devvn_woo_product_rewrite_rules($flash = false) {
	global $wp_post_types, $wpdb;
	$siteLink = esc_url(home_url('/'));
	foreach ($wp_post_types as $type=>$custom_post) {
		if($type == 'product'){
			if ($custom_post->_builtin == false) {
				$querystr = "SELECT {$wpdb->posts}.post_name, {$wpdb->posts}.ID
                            FROM {$wpdb->posts}
                            WHERE {$wpdb->posts}.post_status = 'publish'
                            AND {$wpdb->posts}.post_type = '{$type}'";
				$posts = $wpdb->get_results($querystr, OBJECT);
				foreach ($posts as $post) {
					$current_slug = get_permalink($post->ID);
					$base_product = str_replace($siteLink,'',$current_slug);
					add_rewrite_rule($base_product.'?$', "index.php?{$custom_post->query_var}={$post->post_name}", 'top');
				}
			}
		}
	}
	if ($flash == true)
		flush_rewrite_rules(false);
}
add_action('init', 'devvn_woo_product_rewrite_rules');
/*Fix lỗi khi tạo sản phẩm mới bị 404*/
function devvn_woo_new_product_post_save($post_id){
	global $wp_post_types;
	$post_type = get_post_type($post_id);
	foreach ($wp_post_types as $type=>$custom_post) {
		if ($custom_post->_builtin == false && $type == $post_type) {
			devvn_woo_product_rewrite_rules(true);
		}
	}
}
add_action('wp_insert_post', 'devvn_woo_new_product_post_save');